package com.sunrise.recipenotepad

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_sortRu.setOnClickListener {
            val intent = Intent(this, RecipeList::class.java)
            intent.putExtra(Constants.RECIPE_TYPE, 1)
            startActivity(intent)
        }

        btn_sortUkr.setOnClickListener {
            val intent = Intent(this, RecipeList::class.java)
            intent.putExtra(Constants.RECIPE_TYPE, 2)
            startActivity(intent)
        }

        btn_sortGr.setOnClickListener {
            val intent = Intent(this, RecipeList::class.java)
            intent.putExtra(Constants.RECIPE_TYPE, 3)
            startActivity(intent)
        }
    }
}