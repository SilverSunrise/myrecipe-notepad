package com.sunrise.recipenotepad

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class ActivityRecipe : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe)

        val recipe = intent.getParcelableExtra<Recipe>(RecipeList.INTENT_RECIPE_LIST)
        val recSrc = findViewById<ImageView>(R.id.iv_recipeImg)
        val recTitle = findViewById<TextView>(R.id.tv_recipeTitle)
        val recDesc = findViewById<TextView>(R.id.tv_recipeDescription)

        recSrc.setImageResource(recipe!!.recipeImg)
        recTitle.text = recipe.recipeTitle
        recDesc.text = recipe.recipeDesc

    }
}