package com.sunrise.recipenotepad

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Recipe(
    val recipeImg: Int,
    val recipeTitle: String,
    val recipeDesc: String,
    val recipeShortDesc: String

) : Parcelable