package com.sunrise.recipenotepad

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecipeAdapter(
    private val context: Context,
    private val recipes: List<Recipe>,
    private val hearListenerUnit: (Recipe) -> Unit
) : RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder>() {
    class RecipeViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val recipeImg: ImageView = view.findViewById(R.id.iv_image)
        private val recipeName: TextView = view.findViewById(R.id.tv_title)
        private val recipeDes: TextView = view.findViewById(R.id.tv_shortDescription)

        fun bindView(recipe: Recipe, listener: (Recipe) -> Unit) {
            recipeImg.setImageResource(recipe.recipeImg)
            recipeName.text = recipe.recipeTitle
            recipeDes.text = recipe.recipeShortDesc
            itemView.setOnClickListener { listener(recipe) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder =
        RecipeViewHolder(LayoutInflater.from(context).inflate(R.layout.item_recipe, parent, false))

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        holder.bindView(recipes[position], hearListenerUnit)
    }

    override fun getItemCount(): Int = recipes.size
}